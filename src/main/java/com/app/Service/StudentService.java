package com.app.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.Domain.Human;
import com.app.Repository.HumanRepository;
//defining the business logic
@Service
public class StudentService
{
    @Autowired
    HumanRepository humanRepository;
    //getting all student records
    public List<Human> getAllStudent()
    {
        List<Human> humans = new ArrayList<Human>();
        humanRepository.findAll().forEach(student -> humans.add(student));
        return humans;
    }
    //getting a specific record
    public Human getStudentById(int id)
    {
        return humanRepository.findById(id).get();
    }
    public void saveOrUpdate(Human human)
    {
        humanRepository.save(human);
    }
    //deleting a specific record
    public void delete(int id)
    {
        humanRepository.deleteById(id);
    }
}