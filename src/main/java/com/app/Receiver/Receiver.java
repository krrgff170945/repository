package com.app.Receiver;

import com.app.Domain.Human;
import com.app.Sender.Sender;
import com.app.Service.StudentService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.Integer.parseInt;

@RabbitListener(queues = "c2r")
public class Receiver {

    //autowired the StudentService class
    @Autowired
    StudentService studentService;

    @Autowired
    Human human;

    @Autowired
    Sender sender;

    @RabbitHandler
    public void receive(String message) {
        String[] buffer = message.split("-");

        human.setId(parseInt(buffer[0]));
        human.setCode(parseInt(buffer[1]));
        human.setName(buffer[2]);
        human.setEmail(buffer[3]);

        studentService.saveOrUpdate(human);
        System.out.println(" [x] Received '" +
                human.getId()+"-"+
                human.getCode()+"-"+
                human.getName()+"-"+
                human.getFruit()+"'");

        sender.send("created field with {" +
                "id: " + human.getId()+
                ", code: " + human.getCode()+
                ", name: " + human.getName()+
                ", email: " + human.getFruit());
    }
}
