package com.app.Repository;

import org.springframework.data.repository.CrudRepository;
import com.app.Domain.Human;

public interface HumanRepository extends CrudRepository<Human, Integer>
{
}

